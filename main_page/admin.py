from django.contrib import admin
from .models import *

admin.site.register(MainTextBlock)
admin.site.register(Requisite)
admin.site.register(Position)
