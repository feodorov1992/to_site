import os
from django.shortcuts import render
from .models import *


def main_page(request):
    blocks = MainTextBlock.objects.all()
    return render(request, os.path.join('main_page', 'main.html'), {'blocks': blocks})


def requisites(request):
    requs = Requisite.objects.all()
    return render(request, os.path.join('main_page', 'requisites.html'), {'requisites': requs})


def price_list(request):
    poses = Position.objects.all()
    return render(request, os.path.join('main_page', 'price_list.html'), {'poses': poses})
