from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.main_page, name='main_page'),
    path('requisites/', views.requisites),
    path('price/', views.price_list),
    path('clientsarea/', include('clientsarea.urls')),
]
