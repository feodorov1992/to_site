from typing import Union, List
from openpyxl import load_workbook
from datetime import datetime, date
from decimal import Decimal
import logging

logger = logging.getLogger(__name__)


class ExcelProcessor:
    def __init__(self, file, sheets: Union[int, str, List[str]] = None):
        self.__workbook = load_workbook(file)
        if sheets is None:
            self.__sheet_names = self.__workbook.sheetnames
        elif isinstance(sheets, int):
            self.__sheet_names = [self.__workbook.sheetnames[sheets]]
        elif isinstance(sheets, str):
            self.__sheet_names = [sheets]
        else:
            self.__sheet_names = sheets
        logger.debug(f'File loaded. Prepared to work with sheets: {", ".join(self.__sheet_names)}')

    @staticmethod
    def __validate_str(value: str):
        if isinstance(value, str):
            return ' '.join(value.strip().split())
        elif isinstance(value, float):
            return str(round(value, 3))
        else:
            return str(value)

    @staticmethod
    def __validate_date(value: Union[str, datetime]):
        if isinstance(value, datetime):
            return value.date()
        elif isinstance(value, date):
            return value
        else:
            return datetime.strptime(value.split()[-1], '%d.%m.%Y').date()

    @staticmethod
    def __validate_decimal(value: Union[str, float, int, Decimal]):
        if isinstance(value, Decimal):
            return value
        elif isinstance(value, float):
            return Decimal(round(value, 3))
        else:
            return Decimal(value)

    @staticmethod
    def __validate_int(value: Union[str, int, float]):
        if isinstance(value, int):
            return value
        else:
            return int(value)

    def __read_row(self, row: tuple, mapper):
        validator = {
            'str': self.__validate_str,
            'int': self.__validate_int,
            'date': self.__validate_date,
            'decimal': self.__validate_decimal,
        }
        result = dict()
        errors = list()
        for cell_name, cell_meta in mapper.items():
            cell = row[cell_meta[0]]
            try:
                if cell.value:
                    result[cell_name] = validator[cell_meta[1]](cell.value)
                    if isinstance(result[cell_name], datetime):
                        print(validator[cell_meta[1]])
            except Exception as e:
                errors.append(
                    {
                        'field': cell_name,
                        'field_type': cell_meta[1],
                        'row_index': cell.row,
                        'cell_index': cell_meta[0],
                        'cell_value': cell.value,
                        'msg': e
                    }
                )

        if errors:
            logger.error(errors)
            return errors

        return result

    def __process_worksheet(self, sheet_name: str, mapper: dict):
        logger.debug(f'Processing worksheet "{sheet_name}"')
        worksheet = self.__workbook.get_sheet_by_name(sheet_name)
        result = list()
        rows = worksheet.rows
        next(rows)
        for row in rows:
            row_data = self.__read_row(row, mapper)
            if row_data:
                result.append(row_data)
        return result

    def excel_data(self, mapper):
        result = list()
        for sheet_name in self.__sheet_names:
            result += self.__process_worksheet(sheet_name, mapper)
        return result
