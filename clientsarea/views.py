import datetime
import logging
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import *
from .models import *
from users_auth.injector import inject_form
from django.http import HttpResponseForbidden
from django.core.exceptions import BadRequest


logger = logging.getLogger(__name__)

full_spreadsheet_upload_mapper = {
    'order_manager': (0, 'str'),
    'order_subdivision': (2, 'str'),
    'order_contract': (3, 'str'),
    'order_type': (4, 'str'),
    'order_num': (6, 'str'),
    'order_sub_num': (7, 'str'),
    'order_date': (8, 'date'),
    'order_pick_up_date_plan': (9, 'date'),
    'order_pick_up_date_fact_carrier': (10, 'date'),
    'order_sender': (13, 'str'),
    'order_from_addr': (14, 'str'),
    'order_arr_date_plan': (15, 'date'),
    'order_arr_date_fact': (17, 'date'),
    'order_recipient': (18, 'str'),
    'order_to_addr': (19, 'str'),
    'cargo_name': (20, 'str'),
    'cargo_spaces_num': (22, 'int'),
    'cargo_weight_brut': (25, 'str'),
    'cargo_weight_payed': (26, 'str'),
    'order_status': (42, 'str'),
    'insurance': (33, 'str')
}


manager_spreadsheet_upload_mapper = {

}


def get_or_create(model, **query):
    if not model.objects.filter(**query).exists():
        obj = model(**query)
        obj.save()
    return model.objects.get(**query)


def str_to_obj(data_item, order_key, model, model_key):
    value = data_item.pop(order_key, '')
    if value:
        data_item[order_key] = get_or_create(model, **{model_key: value})


def excel_upload(request):
    from clientsarea.excel_processor import ExcelProcessor
    context = inject_form(request)
    if request.user.is_staff:
        if request.method == 'POST':
            excel_form = ExcelUpload(request.POST, request.FILES)
            if excel_form.is_valid():
                excel_file = request.FILES['file']
                org = excel_form.cleaned_data['org']
                processor = ExcelProcessor(excel_file, 0)
                excel_data = processor.excel_data(full_spreadsheet_upload_mapper)
                new, updated, error = 0, 0, 0
                full_errors = list()
                for item in excel_data:
                    order_full_num = item.pop('order_num', '') + item.pop('order_sub_num', '')
                    item['order_full_num'] = order_full_num
                    str_to_obj(item, 'order_type', OrderType, 'type_label')
                    str_to_obj(item, 'order_status', StatusLabel, 'label')
                    order_arr_date_fact = item.get('order_arr_date_fact')
                    order_pick_up_date_fact_carrier = item.get('order_pick_up_date_fact_carrier')
                    if order_arr_date_fact:
                        item['order_status_date'] = order_arr_date_fact
                    elif order_pick_up_date_fact_carrier:
                        item['order_status_date'] = order_pick_up_date_fact_carrier
                    else:
                        item['order_status_date'] = datetime.date.today()
                    item['order_organisation'] = org
                    if order_full_num:
                        if Order.objects.filter(order_full_num=order_full_num).exists():
                            order = Order.objects.get(order_full_num=order_full_num)
                            changed = False
                            for attr_name, attr_value in item.items():
                                if order.__getattribute__(attr_name) != attr_value:
                                    order.__setattr__(attr_name, attr_value)
                                    changed = True
                            if changed:
                                try:
                                    order.save()
                                    updated += 1
                                except Exception as e:
                                    error += 1
                                    full_errors.append(order_full_num)
                                    logger.error(f'{order_full_num}: {e}')
                                    print(f'{order_full_num}: {e}')
                        else:
                            order = Order(**item)
                            try:
                                order.save()
                                new += 1
                            except Exception as e:
                                error += 1
                                full_errors.append(order_full_num)
                                logger.error(f'{order_full_num}: {e}')
                    else:
                        error += 1
                        logger.error(f'No order num in data: {item}')
                messages.info(request, f'Поручения успешно загружены.')
                messages.info(request, f'Добавлено новых поручений: {new}')
                messages.info(request, f'Обновлено поручений: {updated}')
                messages.info(request, f'Косячных поручений: {error}')
                if full_errors:
                    messages.info(request, f'Некорректные данные по поручениям:\n{", ".join(full_errors)}')

                logger.debug(f'File processed. {new} orders added, {updated} updated, {error} errors')
        else:
            excel_form = ExcelUpload()
        context['excel_form'] = excel_form
        return render(request, 'clientsarea/upload.html', context)
    else:
        return HttpResponseForbidden('Эта страница доступна только для сотрудников ООО "РТЛ-Таможенный оператор"')


def org_info(request):
    context = inject_form(request)
    if request.user.is_authenticated:
        if request.method == 'POST':
            menu = Menu(request.POST)
            ord_filter = OrdersFilter(request.POST)
            if not request.user.is_staff:
                if request.user.profile.org is None:
                    menu.empty()
                else:
                    menu.filter(org=request.user.profile.org)
            if menu.is_valid():
                order_pk = int(menu.data['order'])
                try:
                    order = Order.objects.get(pk=order_pk)
                except models.ObjectDoesNotExist:
                    print(order_pk, repr(menu.data))
                    raise
                # for key, value in order.attrs():
                #     if value is None:
                #         setattr(order, key, '')
                context['order'] = order
                context['order_pk'] = order_pk
            if ord_filter.is_valid():
                cd = ord_filter.cleaned_data
                menu.filter(ord_num=cd['order_num'])
        else:
            menu = Menu()
            ord_filter = OrdersFilter()
            if not request.user.is_staff:
                if request.user.profile.org is None:
                    menu.empty()
                else:
                    menu.filter(org=request.user.profile.org)

        context['menu'] = menu
        context['ord_filter'] = ord_filter
    return render(request, 'clientsarea/clientsarea.html', context)
