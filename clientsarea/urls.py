from django.urls import path
from . import views

urlpatterns = [
    path('', views.org_info),
    path('upload', views.excel_upload, name='upload'),
]
