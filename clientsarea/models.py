from django.db import models
from users_auth.models import Organisation
from colorfield.fields import ColorField
from datetime import date


class OrderType(models.Model):
    type_label = models.CharField(max_length=100, db_index=True)

    def __str__(self):
        return self.type_label

    class Meta:
        verbose_name = "тип поручения"
        verbose_name_plural = "Типы поручений"


class StatusLabel(models.Model):
    label = models.CharField(max_length=255, primary_key=True, verbose_name='Метка', db_index=True)
    color = ColorField(default='#000000', verbose_name='Цвет метки')

    def __str__(self):
        return self.label

    class Meta:
        verbose_name = "тип статуса поручения"
        verbose_name_plural = "Типы статусов поручений"


class Order(models.Model):
    order_manager = models.CharField(max_length=255, verbose_name='Менеджер', blank=True, null=True)
    order_organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE, verbose_name='Заказчик', blank=True, null=True)
    order_subdivision = models.CharField(max_length=255, verbose_name='Подразделение Заказчика', blank=True, null=True)
    order_contract = models.CharField(max_length=255, verbose_name='Договор с Заказчиком', blank=True, null=True)
    order_type = models.ForeignKey(OrderType, on_delete=models.CASCADE, verbose_name='Вид поручения', blank=True, null=True)
    order_full_num = models.CharField(max_length=255, verbose_name='Полный номер Поручения Экспедитора', db_index=True)
    order_date = models.DateField(verbose_name='Дата поручения', blank=True, null=True)
    order_pick_up_date_plan = models.DateField(verbose_name='Дата забора план', blank=True, null=True)
    order_pick_up_date_fact_carrier = models.DateField(verbose_name='Дата забора факт (по данным перевозчика)', blank=True, null=True)
    order_sender = models.CharField(max_length=1500, verbose_name='Наименование и Юр адрес отправителя', blank=True, null=True)
    order_from_addr = models.CharField(max_length=1500, verbose_name='Адрес отправки', blank=True, null=True)
    order_arr_date_plan = models.DateField(verbose_name='Дата доставки план (из Поручения)', blank=True, null=True)
    order_arr_date_fact = models.DateField(verbose_name='Дата доставки Факт', blank=True, null=True)
    order_recipient = models.CharField(max_length=1500, verbose_name='Наименование и юр адрес грузополучателя', blank=True, null=True)
    order_to_addr = models.CharField(max_length=1500, verbose_name='Адрес доставки', blank=True, null=True)
    cargo_name = models.CharField(max_length=1500, verbose_name='Наименование груза', blank=True, null=True)
    cargo_spaces_num = models.IntegerField(verbose_name='Количество грузовых мест', blank=True, null=True)
    cargo_weight_brut = models.CharField(max_length=255, verbose_name='Общий вес груза брутто (кг)', blank=True, null=True)
    cargo_weight_payed = models.CharField(max_length=255, verbose_name='Оплачиваемый вес, кг (по данным перевозчика)', blank=True, null=True)
    order_status = models.ForeignKey(StatusLabel, on_delete=models.CASCADE, verbose_name='Статус', blank=True, null=True)
    order_status_date = models.DateField(verbose_name='Дата обновления статуса', blank=True, null=True)
    order_location = models.CharField(verbose_name='Местоположение', max_length=255, blank=True, null=True)
    insurance = models.CharField(max_length=255, verbose_name='Страхование груза', blank=True, null=True)

    # order_rate = models.FloatField(verbose_name='Стоимость для Заказчика с учетом НДС (Ставка)')
    # order_rate_acked = models.BooleanField(default=True, verbose_name='Ставка согласована')
    # order_currency = models.CharField(max_length=255, verbose_name='Валюта ставки')
    # order_tax = models.IntegerField(default=20, verbose_name='НДС %')
    # order_tax_value = models.FloatField(verbose_name='Сумма НДС')
    # order_value_notax = models.FloatField(
    #     verbose_name='Стоимость для Заказчика в рублях без НДС (валюта пересчитывается по курсу)'
    # )
    # order_pre_rate = models.FloatField(verbose_name='Предварительно согласованная стоимость с Заказчиком ')
    # order_customs_procedure = models.CharField(max_length=255, verbose_name='Таможенная процедура', blank=True)
    # order_description = models.TextField(verbose_name='Примечания')
    # cargo_value = models.FloatField(verbose_name='Стоимость груза')
    # cargo_value_currency = models.FloatField(verbose_name='Валюта стоимости перевозимого груза')
    # cargo_package_type = models.CharField(max_length=255, verbose_name='Вид упаковки')
    # cargo_handling = models.CharField(max_length=255, verbose_name='Характер обработки груза')
    # cargo_danger = models.BooleanField(verbose_name='Опасный')
    # cargo_volume = models.CharField(max_length=255, verbose_name='Обьем, м3')
    # cargo_origin_country = models.CharField(max_length=255, verbose_name='Страна происхожения груза')
    # insurance_value = models.FloatField(verbose_name='Страховая сумма (RUB)')
    # insurance_premium = models.FloatField(verbose_name='Страховая премия')
    # insurance_premium_value = models.CharField(max_length=255, verbose_name='')
    # insurance_premium_currency = models.CharField(
    #     max_length=255, verbose_name='Валюта, в которой рассчитана страховая премия'
    # )
    # insurance_policy_number = models.CharField(max_length=255, verbose_name='Номер страхового полиса')
    # insurance_period = models.CharField(max_length=255, verbose_name='Период страхования')
    # transport_type = models.CharField(max_length=255, verbose_name='Тип транспорта')
    # transport_company = models.CharField(max_length=255, verbose_name='Транспортная компания')
    # transport_contract = models.CharField(max_length=255, verbose_name='Договор перевозчика')
    # transport_order_num = models.CharField(max_length=255, verbose_name='Номер поручения перевозчику')
    # transport_order_date = models.DateField(verbose_name='Дата поручения перевозчику')
    # transport_doc_type = models.CharField(max_length=255, verbose_name='Тип документа')
    # transport_doc_num = models.CharField(max_length=255, verbose_name='Номер перевозочного документа')
    # transport_doc_date = models.DateField(verbose_name='Дата документа')
    # transport_value = models.FloatField(verbose_name='Стоимость перевозчика')
    # transport_currency = models.CharField(max_length=255, verbose_name='Валюта перевозчика')
    # transport_tax = models.IntegerField(default=20, verbose_name='НДС %')
    # transport_tax_value = models.FloatField(verbose_name='Сумма НДС Поставщика в рублях')
    # transport_value_notax = models.FloatField(verbose_name='Стоимость Поставщика без НДС в рублях')
    # transport_doc_send_date = models.DateField(verbose_name='Дата выгрузки ПЭ Кириллом')
    # order_sub_num = models.IntegerField(verbose_name='Субномер')
    # order_pick_up_date_plan_carrier = models.DateField(verbose_name='Дата забора план (по данным перевозчика)')
    # order_delivery_terms = models.TextField(verbose_name='Условия поставки')
    # order_arr_date_plan_carrier = models.DateField(verbose_name='Дата доставки план (по данным перевозчика)')
    # cargo_code = models.CharField(max_length=255, verbose_name='Товарный код (маркировка)')
    # cargo_spaces_num_carrier = models.IntegerField(verbose_name='Колличество мест (по данным перевозчика)')
    # cargo_size = models.CharField(max_length=255, verbose_name='Габариты груза, см')

    def __str__(self):
        return f'Поручение от {self.order_organisation.name} №{self.order_full_num}'

    def attrs(self):
        for field in self._meta.fields:
            yield field.name, getattr(self, field.name)

    class Meta:
        verbose_name = "поручение"
        verbose_name_plural = "Поручения"
