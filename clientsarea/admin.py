from django.contrib import admin
from .models import *


class OrderAdmin(admin.ModelAdmin):
    search_fields = ['order_full_num']


admin.site.register(StatusLabel)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderType)
