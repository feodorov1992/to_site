from django import forms
from .models import Order
from users_auth.models import Organisation


class ExcelUpload(forms.Form):
    org = forms.ModelChoiceField(Organisation.objects.all(), label='Организация')
    file = forms.FileField(label='Файл Excel')


class OrdersFilter(forms.Form):
    order_num = forms.CharField()


class Menu(forms.Form):
    order = forms.ModelChoiceField(
        queryset=Order.objects.all().order_by('-order_status_date'),
        widget=forms.RadioSelect()
    )

    def filter(self, org=None, ord_num=None):
        if org is not None:
            self.fields['order'].queryset = self.fields['order'].queryset.filter(order_organisation=org)
        if ord_num is not None:
            self.fields['order'].queryset = self.fields['order'].queryset.filter(order_full_num__icontains=ord_num)

    def empty(self):
        self.fields['order'].queryset = None
