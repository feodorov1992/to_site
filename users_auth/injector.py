from django.contrib.auth import authenticate, login
from django.contrib import messages
from .forms import LoginForm


def inject_form(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                else:
                    messages.error(request, f"User {cd['username']} is disabled!")
            else:
                messages.error(request, f"Invalid user or password")
    else:
        form = LoginForm()
    return {'form': form}
