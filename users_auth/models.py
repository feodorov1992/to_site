from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Organisation(models.Model):
    name = models.CharField(max_length=100, verbose_name="Сокращенное наименование")
    address = models.CharField(max_length=100, verbose_name="Юридический адрес")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "организацию"
        verbose_name_plural = "Организации"


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=150, null=True, blank=True)
    org = models.ForeignKey(Organisation, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

    class Meta:
        verbose_name = "профиль пользователя"
        verbose_name_plural = "Профили пользователей"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
